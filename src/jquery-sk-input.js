
import { SkInputImpl }  from '../../sk-input/src/impl/sk-input-impl.js';

export class JquerySkInput extends SkInputImpl {

    get prefix() {
        return 'jquery';
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }
}
